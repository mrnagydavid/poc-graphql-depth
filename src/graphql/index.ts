import * as express from "express";
import { ApolloServer } from "apollo-server-express";
import { print, parse } from "graphql";
import { executableSchema } from "./schemas";

const DEPTH_LIMIT = 3;

function depthCheck(query: string) {
  const cleared = query.replace(/[^{}]/g, '');
  const braces = cleared.split('');

  let maxLevel = 0;
  let currentLevel = 0;
  let valid = true;
  for (const brace of braces) {
    switch (brace) {
      case '{':
        ++currentLevel;
        maxLevel = (maxLevel < currentLevel) ? currentLevel : maxLevel;
        break;
      case '}':
        --currentLevel;
        valid = (currentLevel >= 0);
        break;
    }
  }

  return (valid) ? maxLevel : 0;
}

function routeFn({ req } : { req: express.Request | undefined }) {
  if (!req) {
    throw new Error(
      "Apollo GraphQL Initialization Error: GraphQL did not receive the Request object"
    );
  }
  console.log(
    `GraphQL\n${print(parse(req.body.query))}`,
    req.body.variables
  );

  const depth = depthCheck(req.body.query);
  console.log(`GraphQL Depth = ${depth}`);
  console.log(`Query is ${depth <= DEPTH_LIMIT ? "valid" : "invalid" }`);
};

export function createApolloServer() {
  const apolloServer = new ApolloServer({
    schema: executableSchema,
    context: routeFn
  });

  return apolloServer;
}
