import * as express from "express";
import { createApolloServer } from "./graphql/index";

export function server(app: express.Application): express.Application {
  app.use(express.json());
  app.get("/", (_req: express.Request, res: express.Response) =>
    res.send("Hello!")
  );
  
  const apolloServer = createApolloServer();
  apolloServer.applyMiddleware({ app, path: '/graphql' });

  return app;
}
