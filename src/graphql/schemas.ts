import { gql, makeExecutableSchema, IResolvers } from "apollo-server-express";
import { levelSchema, levelResolvers } from "./deepSchema";

const rootSchema = gql`
  type Query {
    _empty: String
  }

  type Mutation {
    _empty: String
  }

  schema {
    query: Query
    mutation: Mutation
  }
`;

const schemas = [
  levelSchema
].map(s => gql(s));

const typeDefs = [
  rootSchema,
  ...schemas,
];

const resolvers = {
  ...levelResolvers,
  Query: {
    ...levelResolvers.Query,
  },
  Mutation: {
    ...levelResolvers.Mutation,
  }
};

export const executableSchema = makeExecutableSchema({
  typeDefs,
  resolvers: resolvers as IResolvers
});