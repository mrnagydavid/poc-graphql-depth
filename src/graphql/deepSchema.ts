const gql = (x: any) => x;

export const levelSchema = gql`
  type Deep {
    id: Int!
    level: Int!
    next: Deep!
  }

  input GoDeeperInput {
    level: Int!
  }

  extend type Query {
    Deep(level: Int!): Deep
  }

  extend type Mutation {
    goDeeper(input: GoDeeperInput!): Deep!
  }
`;

class Deep {
  static _id = 0;
  public id: number = 0;
  constructor(
    public level: number = 0
  ) {
    this.id = Deep._id++;
  }
}

interface IGoDeeperInput {
  level: number;
}

interface IGoDeeperParams {
  input: IGoDeeperInput;
}

export const levelResolvers = {
  Deep: {
    next(
      deep: Deep,
      _args: any,
      _context: any
    ): Deep {
      const nextLevel = (deep && typeof deep.level === "number") ? deep.level + 1 : 0;
      return new Deep(nextLevel);
    },
  },
  Query: {
    Deep(
      _root: any,
      args: any,
      _context: any
    ): Deep {
      return new Deep(args.level);
    }
  },
  Mutation: {
    goDeeper(
      _root: any,
      args: IGoDeeperParams,
      _context: any
    ): Deep {
      return new Deep(args.input.level);
    }
  }
};