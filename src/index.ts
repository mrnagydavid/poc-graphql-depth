import * as express from "express";
import { server } from "./app";

server(express()).listen(3000, () => {
  console.log(`🚀 Server is running!`);
});